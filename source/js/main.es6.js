/*-----------------------------------------
 TABLE OF CONTENT
 1. backToTop
 2. fullWidthCarousel
 3. dataAttrSettingCarousel
 4. groupItemCarousel
 5. lazyloadProduct
 6. countDown
 7. initDropdown for mobile version
 8. smoothScroll
------------------------------------------*/
/*-----------------------------------------
 PLUGIN
 1. jQuery v2.2.5-pre
 2. Bootstrap v4.0.0-beta.2 (https://getbootstrap.com)
 3. Countdown for jQuery v2.1.0 (http://keith-wood.name/countdown.html)
 4. Owl Carousel v2.2.1
 5. popper.js - dependency for Bootstrap v4
 masonry (https://masonry.desandro.com/layout.html)
	
------------------------------------------*/

!function($) {
    "use strict";
    
    $(document).ready( ()=>{
        "use strict";
        let windowWidth = $(window).width();
        /*-----------------------------------------
        1. FOUNDATION
        ------------------------------------------*/
        $(document).foundation();
        
        if(windowWidth < 768){
            var selectFilter = $('.select-filter');
            selectFilter.on('change', ()=> {
                mixer
                    .filter(selectFilter.val())
                    .then((state)=> {
                        console.log(state.activeFilter.selector); // '.category-a'
                        console.log(state.activeFilter.collection.length); // 22
                    });
            });
        }
        if(windowWidth > 960 && $(".likipe-home-page").length){
            var $ticky = new Foundation.Sticky($(".header-inner"), {
                stickyOn: "medium",
                marginTop: 0
            });
            var $magellan = new Foundation.Magellan($("#header-menu .menu"), {
                threshold: 0,
                offset: 96,
            });
        }

        
        
        

        /*-----------------------------------------
        2. FANCYBOX
        ------------------------------------------*/
        $('[data-fancybox="gallery"]').length > 0 && $('[data-fancybox="gallery"]').fancybox({
            buttons: [
                // "zoom",
                //"share",
                //"slideShow",
                //"fullScreen",
                //"download",
                // "thumbs",
                "close"
            ],
            protect: true,
            onInit: function(){},
            afterShow : function( instance, current ) {
                console.info( instance );
            },
            afterLoad : function(instance, current) {
                // var pixelRatio = window.devicePixelRatio || 1;

                // if ( pixelRatio > 1.5 ) {
                //     current.width  = current.width  / pixelRatio;
                //     current.height = current.height / pixelRatio;
                // }
            }
        });

        /*-----------------------------------------
        3. SLICK SLIDER
        ------------------------------------------*/
        $('#hero-slider').length > 0 && $('#hero-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            touchMove: true
        });

        $('.block--icon, .post-block').length > 0 && $(".block--icon, .post-block").each(function(index){
            $(this).find(".media-wrapper img").length > 1 && $(".media-wrapper", this).slick({
                // lazyLoad: 'ondemand',
                arrows: false,
                dots: true,
                slidesToShow: 1,
            })
        });
        
        $('.slider-events').length > 0 && $(".slider-events").slick({
            dots: false,
            infinite: false,
            slidesToShow: (windowWidth < 768)? 1 : 2 ,
            slidesToScroll: 1,
            // adaptiveHeight: true,
            responsive: [{
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
                
            }],
            nextArrow: $('.custom-slider-nav .btn-next'),
            prevArrow: $('.custom-slider-nav .btn-prev')
        });
        
        $("#partner-slider").length > 0 && $("#partner-slider").slick();
        $("#testimonial-slider").length > 0 && $("#testimonial-slider").slick();

        /*-----------------------------------------
        4. COUNTDOWN
        ------------------------------------------*/
        var endDate = new Date("June 7, 2019 15:03:25");
        $('#next-event-countdown').length > 0 && $('#next-event-countdown').countdown({
            date: endDate,
            render: function({years, days, hours, min, sec}) {
                // console.time("countdown");
                $(this.el).find(".countdown-number")
                    .filter("#seconds").text(this.leadingZeros(sec, 2)).end()
                    .filter("#minutes").text(this.leadingZeros(min, 2)).end()
                    .filter("#hours").text(this.leadingZeros(hours, 2)).end()
                    .filter("#days").text(this.leadingZeros(days, 2));
                // console.timeEnd("countdown");    
                    
            },
            onEnd: function() {
                $(this.el).addClass('ended');
            }
        });

        /*-----------------------------------------
        5. MIXITUP - FILTER FOR GALLERY
        ------------------------------------------*/
        var targetSelector = '.mix';
        var initialFilter = 'all';
        /**
         * Reads a hash from the URL (if present), and converts it into a class
         * selector string. E.g "#green" -> ".green". Otherwise, defaults
         * to the targetSelector, equivalent to "all"
         *
         * @return {string}
         */
        function getSelectorFromHash() {
            var hash = window.location.hash.replace(/^#/g, '');
            var selector = hash ? '.' + hash : targetSelector;
            return selector;
        }
        /**
         * Updates the URL whenever the current filter changes.
         *
         * @param   {mixitup.State} state
         * @return  {void}
         */
        function setHash(state) {
            var selector = state.activeFilter.selector;
            var newHash = '#' + selector.replace(/^./g, '');
            if (selector === targetSelector && window.location.hash) {
                // Equivalent to filter "all", remove the hash
                history.pushState(null, document.title, window.location.pathname); // or history.replaceState()
            } else if (newHash !== window.location.hash && selector !== targetSelector) {
                // Change the hash
                history.pushState(null, document.title, window.location.pathname + newHash); // or history.replaceState()
            }
        }

        if($('#gallery-grid').length > 0){
            var mixer = mixitup('#gallery-grid', {
                selectors: {
                    target: targetSelector
                },
                load: {
                    filter: getSelectorFromHash() // Ensure that the mixer's initial filter matches the URL on startup
                },
                callbacks: {
                    onMixEnd: setHash // Call the setHash() method at the end of each operation
                },
                animation: {
                    animateResizeTargets: true
                }
            });
            // Add an "onhashchange" handler to keep the mixer in sync as the user goes
            // back and forward through their history.
            window.onhashchange = ()=> {
                var selector = getSelectorFromHash();
                if (selector === mixer.getState().activeFilter.selector) return; // no change
                mixer.filter(selector);
            };
        }
        

        

        function isMobile() { 
            return ('ontouchstart' in document.documentElement);
        }

        function init_gmap() {
            if ( typeof google == 'undefined' ) return;
            var options = {
                center: [23.709921, 90.407143],
                zoom: 15,
                mapTypeId : google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                },
                navigationControl: true,
                scrollwheel: false,
                streetViewControl: true,
                draggable: isMobile()? false: true
            }

            $('#googleMaps').length > 0 && $('#googleMaps')
                .gmap3({
                    center: [23.709921, 90.407143],
                    zoom: 15,
                    mapTypeId : google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                    },
                    navigationControl: true,
                    scrollwheel: false,
                    streetViewControl: true,
                    draggable: isMobile()? false: true
                })
                .marker({
                    position: [23.709921, 90.407143],
                    icon: 'images/mapicon.png'
                });
        }

        init_gmap();

        $('#scroll-to-top').click(function(){
            $("html,body").animate({ scrollTop: 0 }, 1000);
            return false;
        });
    });

    $(window).on({
        load: ()=>{
            // if($("#loading-wrapper").length){
            //     $("#loading-wrapper").delay(200).fadeOut(500);
            // }
        },  
        scroll: function(){
            if ($(this).scrollTop() > 600) {
                $('#scroll-to-top').fadeIn('slow');
                
            } else {
                $('#scroll-to-top').fadeOut('slow');
            }
        }, 
    });
    
}(jQuery);





